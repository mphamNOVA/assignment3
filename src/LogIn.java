import javax.swing.JOptionPane;

public class LogIn {
	public static void main(String [] agrs) {
	//Correct username & password
	String correctUsername = "abc";
	String correctPassword = "123";
	
	//declare variable: count of attempts & limit. Then use 'do...while' to limit to 3 failing attempts 
	int count = 0;
	final int limit = 3;
	do { //Start loop
		//inputs
		String username = JOptionPane.showInputDialog("Enter your username");
		String password = JOptionPane.showInputDialog("Enter your password");
		
		//Case 1: correct inputs
		if (username.equals(correctUsername)&&password.equals(correctPassword)) {
			//After successfully log in, prompt to select account type
			String[] choices = { "Admin", "Student", "Staff"};
			String accountType = (String) JOptionPane.showInputDialog(null, 
				"Choose account type...","Account Type",JOptionPane.QUESTION_MESSAGE, 
				null,choices,choices[1]); 
			
			//Show welcome message corresponding to account selection
			switch (accountType) {
			case "Student": JOptionPane.showMessageDialog(null, "Welcome student."); break;
			case "Staff": JOptionPane.showMessageDialog(null, "Welcome staff."); break;
			case "Admin": JOptionPane.showMessageDialog(null, "Welcome admin."); break;	
			}
			System.exit(0); //end the program
		
		//Case 2: incorrect inputs. Prompt to suitable instructions.  
		} else { count++; //count the number of failure
			
			//show specific log in problem 
			if (count<3) { 
				if ((!username.equals(correctUsername))&&(!password.equals(correctPassword)))
					JOptionPane.showMessageDialog(null, 
					"Invalid username and password. Please try again.");
				else if (!username.equals(correctUsername))
					JOptionPane.showMessageDialog(null, 
					"Invalid username. Please try again.");
				else if (!password.equals(correctPassword))
					JOptionPane.showMessageDialog(null, 
					"Incorrect password. Please try again.");
			}
			
			//Show message after 3 failing attempts to log in
			else if (count==3) 
					JOptionPane.showMessageDialog(null, "Your account is blocked. "
					+ "Please contact admin.");
		}
	} while (count<limit); //set failure limit. End loop. 
	}
}

